//
//  Personaje.swift
//  Game
//
//  Created by Matias Villarroel on 08/02/17.
//  Copyright © 2017 Miquel Abellán. All rights reserved.
//

import SpriteKit

class Personaje: SKSpriteNode{
    
    let defaultTextureName = "personajee"
    
    
     init() {
        let texture = SKTexture(imageNamed: defaultTextureName)
        let color = UIColor.red
        let size = texture.size()
        super.init(texture: texture, color: color, size: size)
        
        defaultValues()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func defaultValues(){
        
        self.xScale = 1/10
        self.yScale = 1/10
        
        //default physics body values
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width,
                                                                  height: self.size.height))
        
        self.physicsBody!.isDynamic = true
        self.physicsBody!.affectedByGravity = true
        self.physicsBody!.mass = 0.012
        self.physicsBody!.allowsRotation = false
        self.physicsBody!.linearDamping = 0.5
    }
    
    
    func movePersonajeCenter(){
        self.position = CGPoint.zero
    }

    
}
