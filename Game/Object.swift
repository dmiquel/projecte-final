//
//  Object.swift
//  Game
//
//  Created by Matias Villarroel on 08/02/17.
//  Copyright © 2017 Miquel Abellán. All rights reserved.
//

import SpriteKit

class Object: SKSpriteNode {
    
    let defaultTextureName = "objeto"
    
    
    init() {
        let texture = SKTexture(imageNamed: defaultTextureName)
        let color = UIColor.red
        let size = texture.size()
        super.init(texture: texture, color: color, size: size)
        
        defaultValues()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func defaultValues(){
        
        self.xScale = 1/12
        self.yScale = 1/12
    }
    
    
}
