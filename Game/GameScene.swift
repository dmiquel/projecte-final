import SpriteKit
import GameplayKit
import CoreMotion


func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

extension CGPoint {
    func lenght() -> CGFloat {
        return sqrt(x*x+y*y)
    }
}

public func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(Float(arc4random())/Float(UInt32.max))
    }
    
    static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        assert(min < max)
        return CGFloat.random() * (max-min)+min
    }
}


//falta poner las fotos buenas
class GameScene: SKScene{
    
    //let personaje = SKSpriteNode(imageNamed: "personajee") //declaracion del personaje
    let personaje = Personaje()
    var entities = [GKEntity]()
    var graphs = [String : GKGraph]()
    
    var timerCounter = 0
    var pointsCounter = 0
    var nivel = 1
    var timer = Timer()

    var lastUptadeTime: TimeInterval = 0
    var dt: TimeInterval = 0
    let pjPixelsPerSecond: CGFloat = 70.0
    var velocity = CGPoint.zero
    var lastTouchLocation = CGPoint.zero
    private var lastUpdateTime : TimeInterval = 0
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    let gameOverLabel = SKLabelNode()
    var gameOver:Bool = false           //verificar si perdimos
    let youWonLabel = SKLabelNode()
    
    var labelPoints = SKLabelNode()
    
    var labelNivel = SKLabelNode()
    var labelTimer = SKLabelNode()

    let motionManager = CMMotionManager()  //Accelerometro


    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        //color inicio
        backgroundColor = SKColor.green
        
        //foto que va a salir de fondo todo el rato
        let background = SKSpriteNode(imageNamed: "background")
        background.position = CGPoint.zero
        background.zPosition = -2
        addChild(background) // anadimos la imagen para que se vea
    
        //Player
        spawnPlayer()
        
        //Spawn enemies and Objects
        if (!gameOver){
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy), SKAction.wait(forDuration: 5)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnObject), SKAction.wait(forDuration: 6)])))
        }
        //Timer
        iniciarTimer()
        
        //Labels
        anadirLabelLevel()
        anadirLabelTimer()
        anadirPuntacio()
       
        //Accelerometro
        motionManager.startAccelerometerUpdates()
        
        //Boundary --> No se sale de la pantalla
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)

        
    }

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        sceneTouched(touchLocation: location)
        
        if self.atPoint(location) === gameOverLabel || self.atPoint(location) === youWonLabel{
            
            gameOverLabel.removeFromParent()
            youWonLabel.removeFromParent()
            eraseAllNodes(nodeName: "enemigo")
            eraseAllNodes(nodeName: "objeto")
            timerCounter = 15
            nivel = 1
            gameOver = false
            pointsCounter = 0
            actualizarlabelPoints()
            personaje.movePersonajeCenter()
            iniciarTimer()
            
        }
    }
    
    
    //UPDATE
    
    override func update(_ currentTime: TimeInterval) {
        
        //Update Time
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        lastUpdateTime = currentTime
        
        //Collisions
        if (!gameOver){
           checkCollisions()
           checkCollisionsObject()
        }
        
        //Move Player
        if (!gameOver){
            if (personaje.position - lastTouchLocation).lenght() < pjPixelsPerSecond * CGFloat(dt){
                velocity = CGPoint.zero
            } else {
                moveSprite(sprite: personaje, velocity: velocity)
            }
        }
        
        //Accelerometer
        if (!gameOver){
            if let accelerometerData = motionManager.accelerometerData {
                physicsWorld.gravity = CGVector(dx: accelerometerData.acceleration.y * 10, dy: accelerometerData.acceleration.x * -10)
            }else{
                physicsWorld.gravity = CGVector(dx: 0, dy: 0)
            }
        }

        
    }
    
    
    func moveSprite(sprite: SKSpriteNode, velocity: CGPoint) {
        let amount = CGPoint(x: velocity.x * CGFloat(dt), y: velocity.y * CGFloat(dt))
        sprite.position = CGPoint(x: sprite.position.x + amount.x, y: sprite.position.y + amount.y)
    }
    
    func movePjToLocation(location: CGPoint) {
        let offset = CGPoint(x: location.x - personaje.position.x, y: location.y - personaje.position.y)
        let offsetHypo = sqrt(Double(offset.x*offset.x + offset.y*offset.y))
        let direction = CGPoint(x: offset.x/CGFloat(offsetHypo), y: offset.y/CGFloat(offsetHypo))
        velocity = CGPoint(x: direction.x * pjPixelsPerSecond, y: direction.y * pjPixelsPerSecond )
    }
    
    func sceneTouched (touchLocation: CGPoint) {
        lastTouchLocation = touchLocation
        movePjToLocation(location: touchLocation)
        
    }
    
    func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        sceneTouched(touchLocation: location)
        
    }
    
    func checkBounds() {
        
        let bottomLeft = CGPoint(x: -280,y: -150)
        let upperRight = CGPoint(x: 280, y: 150)
        
        if personaje.position.x <= bottomLeft.x {
            personaje.position.x = bottomLeft.x
            velocity.x = -velocity.x
        }
        
        if personaje.position.y <= bottomLeft.y{
            personaje.position.y = bottomLeft.y
            velocity.y = -velocity.y
        }
        
        if personaje.position.x >= upperRight.x {
            personaje.position.x = upperRight.x
            velocity.x = -velocity.x
        }
        
        if personaje.position.y >= upperRight.y {
            personaje.position.y = upperRight.y
            velocity.y = -velocity.y
        }
    }
    
    
    func spawnPlayer (){
        
        personaje.name = "personaje"
        personaje.position = CGPoint.zero
        
        addChild(personaje)
    }
    
    
    
    //Cubo Rubik
    func spawnObject() {
        
        let object = Object()
        object.name = "objeto"
        object.position = posAleatoria()
        
        addChild(object)
    }
    
    
    //ENEMY
    
    func spawnEnemy() {
        
        let enemigo = SKSpriteNode(imageNamed: "enemigo")
        let enemigo2 = SKSpriteNode(imageNamed: "enemigo")

        
        enemigo.name = "enemigo"
        enemigo.position = posAleatoria()
        enemigo.xScale = 1/10
        enemigo.yScale = 1/10
        addChild(enemigo)
        
        let posFinal = CGPoint(x: personaje.position.x, y: personaje.position.y)
        let actionTransaction = SKAction.move(to: posFinal , duration: 10.0-Double(nivel))
        enemigo.run(actionTransaction)
        
        enemigo2.name = "enemigo"
        enemigo2.position = posAleatoria()
        enemigo2.xScale = 1/10
        enemigo2.yScale = 1/10
        addChild(enemigo2)
        
        let actionTransaction2 = SKAction.move(to: posFinal, duration: 10.0-Double(nivel))
        enemigo2.run(actionTransaction2)
        
    }
    
    
    //COLLISIONS
    
    func checkCollisions(){
        
            enumerateChildNodes(withName: "enemigo") {node, _ in
            let enemigo = node as! SKSpriteNode
            if enemigo.frame.intersects(self.personaje.frame){
                self.addGameOverLavel()
                self.gameOver = true
                self.nivel = 1
                self.timer.invalidate()
                self.timerCounter = 10
                self.labelNivel.text = "Level: \(self.nivel)"

            }
        }
        
    }
    func checkCollisionsObject(){
        
        enumerateChildNodes(withName: "objeto") {node, _ in
            let objeto = node as! SKSpriteNode
            if objeto.frame.intersects(self.personaje.frame){
                objeto.removeFromParent()
                self.pointsCounter += 1
                self.actualizarlabelPoints()
                //self.labelNivel.text = "Level: \(self.nivel)"
            }
        }
        
    }
    
    
    //LABELS
    
    func addGameOverLavel() {
        gameOverLabel.fontName = "Pixel-Art"
        gameOverLabel.name = "gameOverLabel"
        gameOverLabel.text = "Game Over!Play again?"
        gameOverLabel.fontSize = 30
        gameOverLabel.fontColor = UIColor.black
        gameOverLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        gameOverLabel.zPosition = 11
        if (!gameOver){
            self.addChild(gameOverLabel)
        }
    }
    
    func addYouWonLabel() {
        youWonLabel.fontName = "Pixel-Art"
        youWonLabel.name = "youWonLabel"
        youWonLabel.text = "You won!Play again?"
        youWonLabel.fontSize = 30
        youWonLabel.fontColor = UIColor.black
        youWonLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        youWonLabel.zPosition = 11
        if (!gameOver){
            self.addChild(youWonLabel)
        }
    }
    
    
    func anadirLabelLevel() {
        labelNivel.fontName = "Pixel-Art"
        labelNivel.name = "labelNivel"
        labelNivel.text = "Level: 1"
        labelNivel.fontSize = 25
        labelNivel.fontColor = UIColor.black
        labelNivel.position = CGPoint(x: self.frame.minX + (labelNivel.frame.maxX + 15),
                                      y: self.frame.maxY - (self.labelNivel.frame.height + 10))
        labelNivel.zPosition = 11
        self.addChild(labelNivel)
    }
    
    func anadirLabelTimer() {
        labelTimer.fontName = "Pixel-Art"
        labelTimer.name = "labelTimer"
        labelTimer.text = "Time: 30"
        labelTimer.fontSize = 25
        labelTimer.fontColor = UIColor.black
        labelTimer.position = CGPoint(x: self.frame.maxX - (labelTimer.frame.maxX + 15),
                                      y: self.frame.maxY - (self.labelTimer.frame.height + 10))
        labelTimer.zPosition = 11
        self.addChild(labelTimer)
    }
    
    func anadirPuntacio() {
        labelPoints.fontName = "Pixel-Art"
        labelPoints.name = "labelTimer"
        labelPoints.text = "Points: 0"
        labelPoints.fontSize = 25
        labelPoints.fontColor = UIColor.black
        labelPoints.position = CGPoint(x: self.frame.minX + 275,
                                       y: self.frame.maxY - (self.labelNivel.frame.height + 10))
        
        labelPoints.zPosition = 11
        self.addChild(labelPoints)
        
    }
    
    
    //MODIFY LABELS
    
    func actualizarLabelTimer() {
        timerCounter -= 1
        labelTimer.text = "Time: \(timerCounter)"
        if (timerCounter == 0) {
            pararTimer()
        }
    }
    
    func actualizarlabelPoints() {
        if (gameOver == true) {
            pointsCounter = 0
        }
        labelPoints.text = "Points: \(pointsCounter)"
        
    }
    
    //TIMER
    
    // ArrancarTimer
    func iniciarTimer() {
        timer.invalidate() // por si se pulsara el boton varias veces
        timerCounter = 15
        // iniciar timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(actualizarLabelTimer), userInfo: nil, repeats: true)
    }
    
    // parar timer
    func pararTimer() {
        timer.invalidate()
        if (nivel < 5) {
            nivel += 1
            iniciarTimer()
            
        }
        else {
            addYouWonLabel()
            gameOver = true
        }
        labelNivel.text = "Level: \(nivel)"
    }
    
    //Borra todos los nodos con un mismo nombre
    func eraseAllNodes(nodeName:String){
        enumerateChildNodes(withName: "\(nodeName)") {node, _ in
            let nodeName = node as! SKSpriteNode
            nodeName.removeFromParent()
        }
    }
    
    //Crea una pos Aleatoria
    func posAleatoria() -> CGPoint{
        
        let random = arc4random_uniform(3)
        var pos: CGPoint
        
        switch random {
            
        case 0:
            pos = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))/2)*(-1) ,
                          y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height)))/2 )
            return pos
            
        case 1:
            pos = CGPoint(x: CGFloat(arc4random_uniform(UInt32(self.frame.size.width))/2),
                          y: CGFloat(arc4random_uniform(UInt32(self.frame.size.height))/2))
            return pos
            
        case 2:
            pos = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))/2)*(-1),
                          y: CGFloat(arc4random_uniform(UInt32(self.frame.size.height))/2)*(-1))
            return pos
            
        default:
            pos = CGPoint(x: CGFloat(arc4random_uniform(UInt32(self.frame.size.width))/2), y: CGFloat(arc4random_uniform(UInt32(self.frame.size.height))/2)*(-1))
            return pos
        }
    }
    
}

